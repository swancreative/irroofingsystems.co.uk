$(document).ready(function() {
    // TOGGLE NAV
    $('.nav__toggle').click(function() {
        $('.nav__list-container').slideToggle();
    });

    // FIX NAV
    function check_fixed_nav() {
        var fixed_nav = $('.nav');
        var scroll_top = $(window).scrollTop();
        var header_bot_pos = $('.header').outerHeight() + $('.header').position().top;
        if (scroll_top >= header_bot_pos) {
            $('body').addClass('nav-fixed');
        } else {
            $('body').removeClass('nav-fixed');
        }
    }
    check_fixed_nav();
    $(window).scroll(function() {
        check_fixed_nav();
    });

    // HEADER BANNER CAROUSEL
    $('.header__banner').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: '<button type="button" class="header__banner-arrow header__banner-arrow--prev"><svg class="header__banner-arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.46 58.29" width="20" height="59"><title>Previous Arrow</title><polygon style="fill: currentColor;" points="0 29.14 15.01 0 19.46 2.29 5.62 29.14 19.46 56 15.01 58.29 0 29.14"/></svg></button>',
        nextArrow: '<button type="button" class="header__banner-arrow header__banner-arrow--next"><svg class="header__banner-arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.46 58.29" width="20" height="59"><title>Next Arrow</title><polygon style="fill: currentColor;" points="19.46 29.14 4.45 0 0 2.29 13.83 29.14 0 56 4.45 58.29 19.46 29.14"/></svg></button>',
        fade: true,
        pauseOnFocus: false,
        pauseOnHover: false,
        speed: 1000
    }).on('afterChange', function(event, slick, currentSlide) {
        $(this).find('.header__banner-title-container').addClass('header__banner-title-container--animate');
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $(this).find('.header__banner-title-container').removeClass('header__banner-title-container--animate');
    });
    $(this).find('.header__banner-title-container').addClass('header__banner-title-container--animate');

    // PAGE CAROUSEL
    TweenLite.set($('.page-carousel__before-after-overlay [data-name="before"]'), { x: '-100%', opacity: 0 });
    TweenLite.set($('.page-carousel__before-after-overlay [data-name="after"]'), { x: '100%', opacity: 0 });
    var tl = new TimelineLite();
    tl.to($('.page-carousel__before-after-overlay [data-name="before"]'), 1, { x: '0%', opacity: 1 });
    tl.to($('.page-carousel__before-after-overlay [data-name="after"]'), 1, { x: '0%', opacity: 1 });

    $('.page-carousel').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: '<button type="button" class="page-carousel__arrow page-carousel__arrow--prev"><svg class="page-carousel__arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.46 58.29" width="20" height="59"><title>Previous Arrow</title><polygon style="fill: currentColor;" points="0 29.14 15.01 0 19.46 2.29 5.62 29.14 19.46 56 15.01 58.29 0 29.14"/></svg></button>',
        nextArrow: '<button type="button" class="page-carousel__arrow page-carousel__arrow--next"><svg class="page-carousel__arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.46 58.29" width="20" height="59"><title>Next Arrow</title><polygon style="fill: currentColor;" points="19.46 29.14 4.45 0 0 2.29 13.83 29.14 0 56 4.45 58.29 19.46 29.14"/></svg></button>',
        pauseOnFocus: false,
        pauseOnHover: false,
        speed: 1000
    }).slick('slickPause').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        TweenLite.set($('.page-carousel__before-after-overlay [data-name="before"]'), { x: '-100%', opacity: 0 });
        TweenLite.set($('.page-carousel__before-after-overlay [data-name="after"]'), { x: '100%', opacity: 0 });
    }).on('afterChange', function(event, slick, currentSlide) {
        tl.restart();
    });

    setTimeout(function() {
        $('.page-carousel').slick('slickPlay');
    }, 2500);

    // TESTIMONIALS CAROUSEL
    $('.testimonials__carousel').slick({
        autoplay: true,
        autoplaySpeed: 5000,
        prevArrow: '<button type="button" class="testimonials__carousel-arrow testimonials__carousel-arrow--prev"><svg class="testimonials__carousel-arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.46 58.29" width="20" height="59"><title>Previous Arrow</title><polygon style="fill: currentColor;" points="0 29.14 15.01 0 19.46 2.29 5.62 29.14 19.46 56 15.01 58.29 0 29.14"/></svg></button>',
        nextArrow: '<button type="button" class="testimonials__carousel-arrow testimonials__carousel-arrow--next"><svg class="testimonials__carousel-arrow-img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.46 58.29" width="20" height="59"><title>Next Arrow</title><polygon style="fill: currentColor;" points="19.46 29.14 4.45 0 0 2.29 13.83 29.14 0 56 4.45 58.29 19.46 29.14"/></svg></button>',
        pauseOnFocus: false,
        pauseOnHover: false,
        speed: 1000
    });

    // UPDATE FOOTER COPYRIGHT YEAR
    $('.footer__year').text(new Date().getFullYear());

    // SCROLL REVEAL ANIMATIONS
    window.sr = ScrollReveal({ scale: 1, distance: 0, duration: 1000 });
    sr.reveal('.icons-row__single', 300);
    sr.reveal('.suppliers__img', 300);
    sr.reveal('.suppliers-footer__img', 200);
    sr.reveal('.two-tone-columns__single', { afterReveal: function(domEl) {
        $(domEl).addClass('two-tone-columns__single--animate');
    } }, 300);

    // GALLERY TABS
    $('.gallery-tabs__item').click(function() {
        $('.gallery-tabs__item').removeClass('gallery-tabs__item--active');
        $(this).addClass('gallery-tabs__item--active');
        $('.gallery').removeClass('gallery--visible');
        $('.gallery[data-gallery-group=' + $(this).text() + ']').addClass('gallery--visible');
    });

    // CONTACT FORM
    $('#form106').on('submit', function(event) {
        var this_form = $(this).attr('id');
        $('#' + this_form + ' .contact-form__error').remove();
        event.preventDefault();
        $.ajax({
            url: 'http://forms.swancreative.com/w/' + $(this).data('wufooformid'),
            data: $(this).serialize(),
            type: 'POST',
            success: function(response) {
                if (response.Success === 1) {
                    $('#' + this_form).after('<p class="contact-form__success">Thank you for contacting us!</p>');
                    $('#' + this_form).remove();
                    window.dataLayer = window.dataLayer || [];
                    window.dataLayer.push({
                      'event': 'contactSuccess'
                    });
                } else {
                    var i = 0;
                    $.each(response.FieldErrors, function() {
                        $('#' + this_form + ' #' + response.FieldErrors[i].ID).after('<p class="contact-form__error">' + response.FieldErrors[i].ErrorText + '</p>');
                        i++;
                    });
                }
            }
        });
    });

    // QUICK QUOTE FORM
    $('#form215').on('submit', function(event) {
        var this_form = $(this).attr('id');
        $('#' + this_form + ' .contact-form__error').remove();
        event.preventDefault();
        $.ajax({
            url: 'http://forms.swancreative.com/w/' + $(this).data('wufooformid'),
            data: $(this).serialize(),
            type: 'POST',
            success: function(response) {
                if (response.Success === 1) {
                    $('#' + this_form).after('<p class="contact-form__success">Thank you for contacting us!</p>');
                    $('#' + this_form).remove();
                } else {
                    var i = 0;
                    $.each(response.FieldErrors, function() {
                        $('#' + this_form + ' #' + response.FieldErrors[i].ID).after('<p class="contact-form__error">' + response.FieldErrors[i].ErrorText + '</p>');
                        i++;
                    });
                }
            }
        });
    });
});
